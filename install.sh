#!/bin/bash
# ADF

# clone tinypilot-jiggle repo (skipped)
#if [ ! -d "/home/pilot/tinypilot/tinypilot-jiggle" ]
#then
#  git clone https://gist.github.com/906534da530482e9afcf1e1309b3dbbe.git
#  mv 906534da530482e9afcf1e1309b3dbbe tinypilot-jiggle
#  chmod +x /home/pilot/tinypilot/tinypilot-jiggle/tinypilot-jiggle.bash
#fi

# make scripts executable
chmod +x ~/tinypilot/tinypilot-jiggle.bash
chmod +x ~/tinypilot/mouse.sh

# create unit file
sudo rm -rf /lib/systemd/system/tinypilot-jiggle.service
sudo touch /lib/systemd/system/tinypilot-jiggle.service
echo "[Unit]" | sudo tee -a /lib/systemd/system/tinypilot-jiggle.service
echo "Description=TinyPilot Jiggler" | sudo tee -a /lib/systemd/system/tinypilot-jiggle.service
echo "" | sudo tee -a /lib/systemd/system/tinypilot-jiggle.service
echo "[Service]" | sudo tee -a /lib/systemd/system/tinypilot-jiggle.service
echo "ExecStart=/home/pilot/tinypilot/tinypilot-jiggle.bash" | sudo tee -a /lib/systemd/system/tinypilot-jiggle.service
echo "" | sudo tee -a /lib/systemd/system/tinypilot-jiggle.service
echo "[Install]" | sudo tee -a /lib/systemd/system/tinypilot-jiggle.service
echo "WantedBy=multi-user.target" | sudo tee -a /lib/systemd/system/tinypilot-jiggle.service

# add, enable, start daemon
sudo systemctl daemon-reload
sudo systemctl enable tinypilot-jiggle.service
sudo systemctl start tinypilot-jiggle.service
sudo systemctl status tinypilot-jiggle.service


# updated root's crontab to enable jiggle service after reboot
(sudo crontab -l; echo "@reboot systemctl enable --now tinypilot-jiggle.service") | sudo crontab -

# bashrc update
echo \
"
# local aliases
alias mouse='~/tinypilot/mouse.sh'
" >> ~/.bashrc

source ~/.bashrc